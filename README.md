# Docker Base image for TYPO3 8.7 and above.

Use this image as reference for your own Dockerfile, for example:

```docker
FROM docker.gitlab.gwdg.de/subugoe/typo3-base:master

COPY . /var/www/html/

RUN php composer.phar install --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative  --no-interaction && \
    php composer.phar clear-cache && \
    # Build Js / CSS
    yarn install && \
    yarn run encore production && \
    # Clean up
    rm -rf /var/www/html/node_modules && \
    chown -R www-data:www-data /var/www/html && \
    mkdir -p /var/www/html/public/typo3temp/assets/_processed_ && \
    chmod -R 777 /var/www/html/public/typo3temp
```