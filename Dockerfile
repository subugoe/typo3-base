FROM php:7.4-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html/public/
ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR /var/www/html/

RUN apt-get update -yqq && \
    echo "Europe/Berlin" > /etc/timezone && \
    rm /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get install -yqq \
        libcurl4-gnutls-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libicu-dev \
        libpng-dev \
        libbz2-dev \
        libzip-dev \
        graphicsmagick \
        apt-transport-https \
        git \
        libxml2-dev \
        libldap2-dev \
        libxslt-dev \
        wget \
        gnupg \
        unzip \
        zlib1g-dev && \
    # Install PHP extensions
    docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install -j$(nproc) \
        intl \
        gd \
        zip \
        ldap \
        soap \
        xsl \
        bz2 \
        opcache \
        mysqli && \
    echo "memory_limit=1024M\nalways_populate_raw_post_data = -1\nmax_execution_time = 240\nmax_input_vars = 1500\nupload_max_filesize = 32M\npost_max_size = 32M" > /usr/local/etc/php/conf.d/memory-limit.ini && \
    printf '[PHP]\ndate.timezone = "Europe/Berlin"\n' > /usr/local/etc/php/conf.d/tzone.ini && \
    pecl install apcu && \
    docker-php-ext-enable apcu soap xsl&& \
    # Node and yarn
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    curl -fsSL https://deb.nodesource.com/setup_22.x -o nodesource_setup.sh && \
    bash nodesource_setup.sh && \
    apt-get update -yqq && \
    apt-get install -yqq nodejs && \
    # Install and run Composer
    curl -sS https://getcomposer.org/installer | php && \
    # Clean up
    docker-php-source delete && \
    rm -rf /var/lib/apt/lists/* && \
    sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf && \
    sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf && \
    { \
    echo '<IfModule http2_module>'; \
    echo '  Protocols h2 h2c http/1.1'; \
    echo '  H2Direct on'; \
    echo '</IfModule>'; \
    } >> /etc/apache2/sites-enabled/000-default.conf && \
    a2enmod ssl headers rewrite
